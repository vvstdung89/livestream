var async = require('async');
var SessionService = require("./app/logics/Thrift/SessionService");
var ChannelService = require("./app/logics/Thrift/ChannelService");

function parseCookies(request) {
	var list = {},
		rc = request.headers.cookie;
	rc && rc.split(';').forEach(function( cookie ) {
		var parts = cookie.split('=');
		list[parts.shift().trim()] = decodeURI(parts.join('='));
	});
	return list;
}

function getCurrentChannel(locals, res, channelID, channelList, callback){
    var check = false;
    
    for (var i in channelList.channels){
        if (channelList.channels[i].id == channelID){
            check = true;
            break;
        }
    }
    if (!check){
        res.cookie('channel', channelList.channels[0].id);
        locals.channelID = channelList.channels[0].id
    } else
        locals.channelID = channelID

    callback()
}

exports = module.exports = function (app, router, redis_client){

	router.use(function(req, res, next) {
        async.auto({
            checkSession: function(callback){
                var sessionID = parseCookies(req)["meme.cloud"];
                var channelID = parseCookies(req)["channel"];
                SessionService.read.checkSession(sessionID, function(err, result){
                    if ( !err && result.error == 0 ){
                        res.locals = {
                            userID: result.id,
                            userEmail: result.email
                        };
                        res.redis_client = redis_client;
                        res.app = app;
                        ChannelService.read.getChannelOfUser(result.id,0,10000,function(err, result){
                            //TODO: check if error
                            
                            getCurrentChannel(res.locals, res, channelID, result, function(){
                                next()
                            });

                        })
                    } else {
                        res.redirect("/")
                    }
                    callback();
                });
            }
        });


	});
};
