function removeDatePicker() {
    $('.xdsoft_datetimepicker').remove();
    mmCommon.removeEventListener(mmEvent.CLOSE_CURRENT_PAGE, removeDatePicker);
}
mmCommon.addEventListener(mmEvent.CLOSE_CURRENT_PAGE, removeDatePicker);
var renderLoadding = {
    loading: function(obj, css) {
        obj.css(css);
        obj.find('.loader').remove();
        obj.append('<div class="loader" style="float: right;margin: auto;margin-top: 10px;margin-left: 10px;"></div>');
    },
    removeLoading: function(obj) {
        obj.find('.loader').remove();
    },
};
var showMessage = function(error, title, message) {
    var mm = new mmMessage({
        error: error,
        timeOut: 5000,
        title: title,
        message: message,
    });
    mm.doAlert();
}
;
var renderContent = function() {
    if (typeof $('.selectType').SumoSelect !== 'undefined' && $.isFunction($('.selectType').SumoSelect)) {
        $('#type-select-none').hide();
        $('.selectType').show();
        $('.selectType').SumoSelect({
            placeholder: meCloudLANGUAGE.video.chooseTypes,
            csvDispCount: 4,
            captionFormat: meCloudLANGUAGE.video.selectedTypes,
        });
    }
    $('.selectType').change(function() {
        $(this).closest('div.mm-type').find('label.error-upload').remove();
        if ($(this).find("option:selected").length > 3 || $(this).find("option:selected").length == 0) {
            var errorMsg = meCloudLANGUAGE.error._017;
            if ($('.SumoSelect').find("option:selected").length == 0) {
                errorMsg = meCloudLANGUAGE.error._018;
            }
            $(this).closest('div.mm-type').append('<label for="types_ids" class="error-upload">' + errorMsg + ' <i class="fa fa-level-up"></i></label>');
        }
    });
    if (typeof $('.selectCategory').SumoSelect !== 'undefined' && $.isFunction($('.selectCategory').SumoSelect)) {
        $('#category-select-none').hide();
        $('.selectCategory').show();
        $('.selectCategory').SumoSelect({
            placeholder: meCloudLANGUAGE.video.chooseCategories,
            csvDispCount: 4,
            captionFormat: meCloudLANGUAGE.video.selectedCategories,
        });
    }
    $('.selectCategory').change(function() {
        $(this).closest('div.mm-category').find('label.error-upload').remove();
        if ($(this).find("option:selected").length > 3) {
            var errorMsg = meCloudLANGUAGE.error._019;
            $(this).closest('div.mm-category').append('<label for="types_ids" class="error-upload">' + errorMsg + ' <i class="fa fa-level-up"></i></label>');
        }
    });
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || ((element.files[0].size / 1024 / 1024) <= param)
    });
    if (typeof $('#tags').tagsInput !== 'undefined' && $.isFunction($('#tags').tagsInput)) {
        $("#tags-none").hide();
        $("#tags").show();
        $('#tags').tagsInput({
            'height': '103px',
            'width': '100%',
            'defaultText': meCloudLANGUAGE.video.addTags,
            'placeholderColor': '#999999',
            'delimiter': [','],
            'minChars': 2,
            'maxChars': 30,
            'maxTags': 20,
        });
    }
    $('body').click(function(e) {
        var clickedOn = $(e.target);
        var placeholder = meCloudLANGUAGE.video.ex + ": http://trangweb.vn/video-hay.html";
        if (clickedOn.parents().andSelf().is('#live-url')) {
            $('#live-url').attr('placeholder', '');
        } else {
            $('#live-url').attr('placeholder', placeholder);
        }
    });
    $('ul.tabs li a').click(function(e) {
        e.preventDefault();
        var currentTab = $(this).attr('href');
        var showTab = true;
        if (currentTab == '#subtitles' || currentTab == '#advanceUpload') {
            var videoId = $('#videoId').val();
            if (videoId == '') {
                showTab = false;
                var p = new Popup({
                    title: $('#popup-warning').attr('title'),
                    content: $('#popup-warning').html()
                });
                p.show();
            }
        }
        if (showTab == true) {
            if (currentTab == '#subtitles') {
                $('.actionForm').find('button.btn.btnPrimary').hide();
            } else {
                $('.actionForm').find('button.btn.btnPrimary').show();
            }
            $('.tabs li, .tabsContentGlobal .current').removeClass('current');
            $(this).parent().addClass('current');
            $(currentTab).addClass('current');
        }
    });
    if (typeof $('#published-time').datepicker !== 'undefined' && $.isFunction($('#published-time').datepicker)) {
        $('#published-time').datepicker();
    }
    
}
;
window.mmLive = function() {
    this.initialize();
}
window.mmLive.prototype = {
    initialize: function() {
        renderContent();
        $("#idFormEdit").validate({
            ignore: [],
            errorClass: 'error-upload',
            rules: {
                title: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                types_ids: {
                    required: true,
                    maxlength: 3
                },
                categories_ids: {
                    maxlength: 3
                },
                description: {
                    minlength: 10,
                    maxlength: 1000
                },
                image_file: {
                    accept: "image/*",
                    filesize: 5
                },
                live_url: {
                    url: true,
                    maxlength: 150
                }
            },
            messages: {
                title: {
                    required: meCloudLANGUAGE.error._013 + " <i class=\"fa fa-level-up\"></i>",
                    minlength: meCloudLANGUAGE.error._013 + " <i class=\"fa fa-level-up\"></i>",
                    maxlength: meCloudLANGUAGE.error._013 + " <i class=\"fa fa-level-up\"></i>"
                },
                types_ids: {
                    required: meCloudLANGUAGE.error._018 + " <i class=\"fa fa-level-up\"></i>",
                    maxlength: meCloudLANGUAGE.error._017 + " <i class=\"fa fa-level-up\"></i>"
                },
                categories_ids: {
                    maxlength: meCloudLANGUAGE.error._019 + " <i class=\"fa fa-level-up\"></i>"
                },
                description: {
                    minlength: meCloudLANGUAGE.error._012 + " <i class=\"fa fa-level-up\"></i>",
                    maxlength: meCloudLANGUAGE.error._012 + " <i class=\"fa fa-level-up\"></i>"
                },
                image_file: {
                    accept: meCloudLANGUAGE.error._024,
                    filesize: meCloudLANGUAGE.error._014
                },
                live_url: {
                    url: meCloudLANGUAGE.error._021 + " <i class=\"fa fa-level-up\"></i>",
                    maxlength: meCloudLANGUAGE.error._022 + " <i class=\"fa fa-level-up\"></i>"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("id") == "types_ids") {
                    error.appendTo(".mm-type");
                } else if (element.attr("id") == "categories_ids") {
                    error.appendTo(".mm-category");
                } else if (element.attr("id") == "image_file") {
                    error.insertAfter(".thumbUpload");
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    popUpHelpUpload: function() {
        var content = '<p class="helpRequire">' + meCloudLANGUAGE.video.video_upload_link_should_be_made_public_can_upload_mecloud + '</p>' + '<p class="helpRequire">' + meCloudLANGUAGE.video.copy_the_link_in_the_address_bar_of_the_browser_when_viewing_the_video_to_upload + '</p>' + '<p class="helpRequire">' + meCloudLANGUAGE.video.see_some_examples_below + ':</p>' + '<div class="exampleLinkUpload">' + '<span class="iconYoutube"><i class="fa fa-youtube-play"></i></span>' + '<p>https://www.youtube.com/watch?v=D3QIfsX1VAU</p>' + '</div>' + '<div class="exampleLinkUpload">' + '<span class="iconFacebook"><i class="fa fa-facebook"></i></span>' + '<p>https://www.facebook.com/1155047347/videos/vb.1155047347/10206231880613348/?type=2&theater </p>' + '</div>' + '<div class="exampleLinkUpload">' + '<span class="iconVimeo">' + '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' + 'width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">' + '<g>' + '<path fill-rule="evenodd" clip-rule="evenodd" d="M19.907,4.835c0.149-0.837,0.146-1.698-0.371-2.35' + 'c-0.722-0.913-2.257-0.947-3.309-0.785C15.37,1.83,12.475,3.108,11.49,6.165c1.745-0.133,2.66,0.126,2.492,2.047' + 'c-0.07,0.804-0.476,1.685-0.93,2.529c-0.524,0.973-1.505,2.885-2.793,1.507c-1.16-1.24-1.072-3.612-1.338-5.191' + 'c-0.148-0.886-0.304-1.99-0.594-2.902c-0.25-0.784-0.824-1.729-1.526-1.933C6.049,2.002,5.118,2.347,4.571,2.67' + 'c-1.742,1.024-3.07,2.48-4.577,3.682v0.112C0.293,6.75,0.373,7.218,0.813,7.281C1.85,7.434,2.839,6.315,3.528,7.48' + 'c0.419,0.712,0.55,1.492,0.819,2.259c0.359,1.022,0.636,2.135,0.93,3.31c0.497,1.99,1.108,4.964,2.828,5.693' + 'c0.878,0.373,2.197-0.126,2.865-0.522c1.809-1.071,3.219-2.623,4.426-4.202C18.155,10.279,19.677,6.045,19.907,4.835z"/>' + '</g>' + '</svg>' + '</span>' + '<p>https://vimeo.com/channels/staffpicks/126897736</p>' + '</div>' + '<div class="exampleLinkUpload">' + '<span class="iconZingtv">' + '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' + 'width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">' + '<path fill-rule="evenodd" clip-rule="evenodd" d="M3.743,0.433c3.413-0.076,12.114-1.058,13.202,1.293' + 'c0.676,2.73-7.871,11.875-9.43,14.511c3.065-0.03,7-0.341,9.576,0.287c0.565,1.046,0.801,2.521-0.435,3.161' + 'c-1.321,0.683-13.204,0.125-13.783-0.431c-0.358-0.225-0.453-0.432-0.725-0.718c0.035-3.292,7.665-11.651,9.575-14.511' + 'c-2.756-0.048-5.513-0.096-8.27-0.144c-0.338-0.335-0.677-0.67-1.016-1.006c0.048-0.431,0.097-0.862,0.145-1.293' + 'C2.88,0.941,3.21,0.819,3.743,0.433z"/>' + '</svg>' + '</span>' + '<p>http://tv.zing.vn/video/Bo-Oi-Minh-Di-Dau-The-Viet-Nam-Tap-28/IWZBUFI0.html</p>' + '</div>';
        var p = new Popup({
            title: meCloudLANGUAGE.video.instructions_uploads_by_link,
            content: content,
            width: 750,
        });
        p.show();
    },
    formSubmit: function(obj) {
        var videoId = $('#videoId').val();
        if (videoId != '' && videoId > 0) {
            $('#idFormEdit').submit();
        } else {
            customAlert(meCloudLANGUAGE.error._016);
            return false;
        }
    },
    isEmpty: function(value) {
        return ( typeof value === "undefined" || value === null ) ;
    },
    loadImage: function(obj) {
        var file = obj[0].files;
        var maxSize = 5;
        var extensions = ["jpg", "jpeg", "gif", "png"];
        var check_image = mmLive.checkFileSize(file, maxSize, extensions);
        if (check_image == -1) {
            showMessage(true, meCloudLANGUAGE.success._002, meCloudLANGUAGE.error._024);
            return;
        }
        if (check_image == -2) {
            showMessage(true, meCloudLANGUAGE.success._002, meCloudLANGUAGE.error._014);
            return;
        }
        if (file && file[0]) {
            obj.closest('div').find('img').remove();
            obj.closest('div').append('<img style="float:right;margin-right:0;" src="' + mmConfig.STATIC_DOMAIN + '/images/preloader.gif" width="22" height="22" />');
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
                obj.closest('div.thumbUpload').find('img').attr('src', e.target.result);
                obj.closest('div').find('img').remove();
            }
            ;
            filerdr.readAsDataURL(file[0]);
        }
    },
    checkFileSize: function(file, maxSize, extentionsArray) {
        var val = file[0].name;
        var ext = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
        if ($.inArray(ext, extentionsArray) == -1) {
            return -1;
        }
        var fileSize = (file[0].size / 1024 / 1024);
        if (fileSize > maxSize) {
            return -2
        }
        return 0;
    },
    addCategory: function() {
        var p = new Popup({
            title: meCloudLANGUAGE.video.addCategories,
            content: '<form id="idFormCategory" class="form-horizontal" action="" method="post">' + '<div class="formGroup">' + '<input id="name" name="name" type="text" placeholder="' + meCloudLANGUAGE.video.name + '" class="formGlobal" />' + '<input name="channelId" id="channelId" type="hidden" value="' + mmConfig.channelId + '" />' + '</div>' + '<div class="actionForm mlr-20 clearfix">' + '<button type="submit" class="btn btnPrimary btn-submit">' + meCloudLANGUAGE.video.save + '</button>' + '<button onclick="$(\'.popup-exit\').click();" type="button" class="btn btnDefault">' + meCloudLANGUAGE.video.cancel + '</button>' + '</div>' + '</form>'
        });
        p.show();
        $('#idFormCategory').validate({
            errorClass: 'error-upload',
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                }
            },
            messages: {
                name: {
                    required: meCloudLANGUAGE.error._025 + "<i class=\"fa fa-level-up\"></i>",
                    minlength: meCloudLANGUAGE.error._025 + " <i class=\"fa fa-level-up\"></i>",
                    maxlength: meCloudLANGUAGE.error._025 + " <i class=\"fa fa-level-up\"></i>"
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form) {
                $.ajax({
                    type: 'POST',
                    url: mmConfig.HOST + '/video/action/add-category-ajax',
                    dataType: "json",
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    error: function(jqXHR, textStatus, errorThrown) {
                        customAlert(meCloudLANGUAGE.error._020);
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.error != 0) {
                            customAlert(data.data);
                        } else {
                            var temp = $('select#categories_ids');
                            var num = temp.find('option').length;
                            temp[0].sumo.add(data.data, form.elements['name'].value, num);
                            temp[0].sumo.selectItem(num);
                            showMessage(false, meCloudLANGUAGE.success._002, meCloudLANGUAGE.success._001);
                            $('#meme-popup-exit').click();
                        }
                        return false;
                    }
                });
                return false;
            }
        });
    },
}
window.mmLive = new mmLive();
