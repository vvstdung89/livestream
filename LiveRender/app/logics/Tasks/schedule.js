var LiveAPI = require("../../../Services/LiveStream.js");
var path = require("path");
var fs = require("fs");
var os = require("os");
var async = require("async");
var VideoService = require("../Thrift/VideoService");
var execSync = require("child_process").execSync;
var Logger = require("../libs/Logger")

var nextSchedule;
//periodicaly get schedule 
function updateSchedule(){
	console.log("Running UpdateSchedule ...")
	LiveAPI.getNextSchedule(function(err, result){
		if (!err) {
			var resultObj = JSON.parse(result)
			if (resultObj.status=="000"){
				if (resultObj.data && resultObj.data.aliasId && resultObj.data.key){
					console.log("Running UpdateSchedule ... ")
					console.log(resultObj.data)
					var curTime = (new Date()).getTime()
					var scheduleTime = resultObj.data.schedule - curTime
					if (scheduleTime < 0) scheduleTime = 1
					clearTimeout(nextSchedule)
					var nextSchedule = setTimeout(function(){
						var info = {
							key: resultObj.data.key,
							aliasId: resultObj.data.aliasId,
							publish: true,
							events: [(new Date()).getTime()]
						}

						info.activity = [{
				            time: (new Date()).getTime(),
				            name: 2
				        }]

						LiveAPI.update(info, function(err , result){
							console.log(err || result)
							updateSchedule()
						})

						

					}, scheduleTime)
				}
			} else {
				Logger.info("Error: " + result)
			}
		} else
			Logger.info("Error: " + err.message)
	})
}

setInterval(function(){
	updateSchedule()
},60*1000)


exports = module.exports = {
	update: function(){
		updateSchedule()	
	}
}

