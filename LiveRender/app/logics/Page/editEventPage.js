var Logger = require("../libs/Logger.js")
var STATUS = require("../libs/ErrorCode.js")
var LiveAPI = require("../../../Services/LiveStream.js")

exports = module.exports = function(req, res){
	var key = req.params.key;
	var locals = {}
	var info = {
		key: key
	}
	console.log(info)
	LiveAPI.get(info, function(err, result){
		console.log(err || result)
		if (err) {
			res.render("Page/Error")
			return;			
		}

		result = JSON.parse(result)
		if (result.status != 0) {
			res.render("Page/Error")
			return;
		}
		var event = result.data;
		console.log(event)
		locals.title = event.title
		locals.key = event.key
		locals.image = "//imgs.cloud.memefiles.com/videoDefault-1.01.jpg"
		res.render("Page/editEvent",locals)	
	})

	
}