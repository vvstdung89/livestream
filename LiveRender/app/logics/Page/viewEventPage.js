var Logger = require("../libs/Logger.js");
var STATUS = require("../libs/ErrorCode.js");
var LiveAPI = require("../../../Services/LiveStream.js");
var async = require("async")
var VideoService = require("../Thrift/VideoService");
var RealtimeService = require("../Thrift/RealtimeService");

exports = module.exports = function(req, res){
	var aliasId = req.params.key;
	var locals = {};
	var info = {
		aliasId: aliasId
	};
	console.log(info);

	async.auto({
		videoInfo: function(callback){
			VideoService.read.getVideoByAlias(aliasId, function(err, result){
				if (err){
					callback(true,{msg: "Thrift error"});
					return;
				}
				if (result.error!=0){
					callback(true, result);
					return;
				}
				callback(null, result.video)
			})
		},
		liveInfo: function(callback){
			LiveAPI.get(info, function(err, result){
				console.log(err || result);
				if (err) {
					callback(err);
					return;
				}

				result = JSON.parse(result);
				if (result.status != 0) {
					callback(result)
					return;
				}
				callback(null, result.data)
			})
		},
		realtime: ["videoInfo", function(results, callback){
            var videoInfo = results.videoInfo;
            RealtimeService.read.getVideo(Number(videoInfo.id), function(err, result){
                console.log(err || result);
                try{
                    if (err || !result || result.error!=0){
                        callback(null, 0);        
                    } else{
                        callback(null, result.data.play);    
                    }    
                } catch(err){
                    callback(null, 0);        
                }
            })
        }],
		response: ["realtime","videoInfo","liveInfo", function(results, callback){
			var videoInfo = results.videoInfo;
			var liveInfo = results.liveInfo;
			var realtime = results.realtime;

			locals.realtime = realtime;
			if (liveInfo.signal==false)
				locals.statusLabel = "Signal Off"
			else
				locals.statusLabel = "Signal On"

			if (liveInfo.publish == false){
				locals.liveButton = "Start Live"
				if (liveInfo.schedule){
					locals.schedule = true
					var pushlishTime = new Date(liveInfo.schedule);
					locals.hour = (pushlishTime.getHours())+"";
				} else { // no pushlish
					locals.schedule = false
					var pushlishTime = new Date();
					locals.hour = (pushlishTime.getHours()+1)+"";
				}
				
				locals.minute = pushlishTime.getMinutes()+"";
				if (locals.minute.length==1) locals.minute = "0"+locals.minute;
				if (locals.hour.length==1) locals.hour = "0"+locals.hour;
				locals.date = pushlishTime.getDate() + "/" + (pushlishTime.getMonth()+1) + "/" + (pushlishTime.getYear()+1900);
			}
			else if (liveInfo.state!="ended") { // not pushlish and no schedule
				locals.schedule = true
				var pushlishTime = new Date(liveInfo.events[0]);
				locals.hour = (pushlishTime.getHours())+"";
				locals.minute = pushlishTime.getMinutes()+"";
				if (locals.minute.length==1) locals.minute = "0"+locals.minute;
				if (locals.hour.length==1) locals.hour = "0"+locals.hour;
				locals.date = pushlishTime.getDate() + "/" + (pushlishTime.getMonth()+1) + "/" + (pushlishTime.getYear()+1900);
				locals.liveButton = "End Live"
			} else {
				locals.schedule = true
				var pushlishTime = new Date(liveInfo.events[0]);
				locals.hour = (pushlishTime.getHours())+"";
				locals.minute = pushlishTime.getMinutes()+"";
				if (locals.minute.length==1) locals.minute = "0"+locals.minute;
				if (locals.hour.length==1) locals.hour = "0"+locals.hour;
				locals.date = pushlishTime.getDate() + "/" + (pushlishTime.getMonth()+1) + "/" + (pushlishTime.getYear()+1900);
				locals.liveButton = "Ended"
			}

			locals.title = videoInfo.title;
			locals.description = videoInfo.description;
			locals.channelId = videoInfo.channelId;
			locals.aliasId = aliasId;
			if (videoInfo.thumbnail){
				locals.imagePath = "//imgs.cloud.mecloud.vn/thumb/clip/" + videoInfo.thumbnail;
			} else {
				locals.imagePath = "//thumb.imgs.mecloud.vn/videoDefault-1.01.jpg";
			}
			
			locals.rtmp_url = "rtmp://mecloud.vn:1935/live";
			locals.activity = JSON.stringify(liveInfo.activity)

			locals.key = liveInfo.key;
			locals.state = liveInfo.state;
			locals.signal = liveInfo.signal;

			if (locals.state=="end")
				locals.publish="ended"
			else
				locals.publish = liveInfo.publish || false;

			



			res.render("Page/viewEvent2",locals);
		}]
	}, function(err, results){
		console.log(err || results);
	})


	
};