/**
 * Created by david89 on 9/14/2016.
 */
var thrift = require('thrift');
var ChannelService_READ = require("../../../Services/ChannelService/MemeChannel_Read");

var transport = require('thrift/lib/nodejs/lib/thrift/framed_transport.js');
var protocol  = require('thrift/lib/nodejs/lib/thrift/binary_protocol.js');

var ChannelRead = thrift.createClient(ChannelService_READ, thrift.createConnection("mecloud.vn",9027, {
    transport : transport,
    protocol : protocol
}));


exports = module.exports = {
    read: ChannelRead,
    type: require("../../../Services/ChannelService/memecloud.channel_types")
};
