/**
 * Created by david89 on 9/14/2016.
 */
var thrift = require('thrift');
var PacketService_READ = require("../../../Services/PacketService/MeCloudPacketService");

var transport = require('thrift/lib/nodejs/lib/thrift/framed_transport.js');
var protocol  = require('thrift/lib/nodejs/lib/thrift/binary_protocol.js');

var PacketClient = thrift.createClient(PacketService_READ, thrift.createConnection("mecloud.vn",9720, {
    transport : transport,
    protocol : protocol
}));


exports = module.exports = {
    read: PacketClient,
    type: require("../../../Services/PacketService/mecloud.packet_types")
};
