var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js")
var path = require("path")
var fs = require("fs")
var os = require("os")
var async = require("async")
var UpdateSchedule = require("../Tasks/schedule.js")
exports = module.exports = function(req, res){

	var publish = "";
	var aliasId = "";
	var busboy = new Busboy({ headers: req.headers });

	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
		console.log(fieldname + " --- " + val)
		if (fieldname=="publish" && val) publish = val;
		if (fieldname=="aliasId" && val) aliasId = val;
	});

	busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
		var time = new Date();
		imagePath = path.join(os.tmpDir(), path.basename(time.getTime()));
		file.pipe(fs.createWriteStream(imagePath));
	});

	busboy.on('finish', function() {

		if (!aliasId){
			res.json("{}")
			return;
		}

		if (!publish){
			res.json("{}")
			return;
		}
		var info = {
			aliasId: aliasId,
			publish: publish=="true",
			events: [(new Date()).getTime()]
		}

		info.activity = [{
            time: (new Date()).getTime(),
            name: 2
        }]

		LiveAPI.update(info, function(err , result){
			console.log(err || result)
			if (!err) {
				if (result.status == 0){
					res.json(result.data)
					UpdateSchedule.update()
					return;
				}
			}
			res.json("{}")
		})

	});

	req.pipe(busboy);
}