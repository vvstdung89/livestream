var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js");
var VideoService = require("../Thrift/VideoService");
var PacketService = require("../Thrift/PacketService");

var path = require("path")
var fs = require("fs")
var os = require("os")
var async = require("async")
var Gearman = require("node-gearman");
var config = require("../../../Config.js")
var util = require("../libs/Common.js")
var hostname = config.gearman_config.host
var port = config.gearman_config.port
var gearman = new Gearman(hostname, port);
gearman.on("connect", function(){});
gearman.connect();

exports = module.exports = function(req, res){

	var title = "";
	var timing = "0";
	var imagePath = ""
	var channelId = res.locals.channelID
    var types_ids = [];
    var date,minute, hour;
    var aliasId = "";
	var busboy = new Busboy({ headers: req.headers });
               
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        console.log(fieldname + " --- " + val)
        if (fieldname=="channelId" && val) channelId = val;
        if (fieldname=="timing" && val) timing = val;
        if (fieldname=="title" && val) title = val;
        if (fieldname=="types_ids" && val) types_ids.push(val);
        if (fieldname=="date" && val) date = val;
        if (fieldname=="hour" && val) hour = val;
        if (fieldname=="minute" && val) minute = val;
        if (fieldname=="aliasId" && val) aliasId = val;
    });

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    	var time = new Date();
		imagePath = path.join(os.tmpDir(), path.basename(time.getTime()));
		file.pipe(fs.createWriteStream(imagePath));
    });

    busboy.on('finish', function() {

    	if (!aliasId){
			res.json("{}")
            return;
        }

        var time = (new Date()).getTime()
    	var info = {
			aliasId: aliasId,
    		state: "end",
            events: [time]
    	}

        res.header("Access-Control-Allow-Origin","*")
        async.auto({
            videoInfo: function(callback){
                VideoService.read.getVideoByAlias(aliasId, function(err, result){
                    if (err){
                        callback(true,{msg: "Thrift error"});
                        return;
                    }
                    if (result.error!=0){
                        callback(true, result);
                        return;
                    }
                    callback(null, result.video)
                })
            },
            liveInfo: function(callback){
                LiveAPI.get(info, function(err, result){
                    // console.log(err || result);
                    if (err) {
                        callback(err);
                        return;
                    }

                    result = JSON.parse(result);
                    if (result.status != 0) {
                        callback(result)
                        return;
                    }
                    callback(null, result.data)
                })
            },
            updateLive: function(callback) {
                callback(null);
                info.activity = [{
                    time: (new Date()).getTime(),
                    name: 3
                }]
                LiveAPI.update(info, function (err, result) {
                    console.log(err || result)
                    if (!err) {
                        if (result.status == 0) {
                            res.json(result.data)
                            return;
                        }
                    }
                    res.json("{}")
                })
            },
            getPlan: function(callback){
                console.log("get Plan")
                PacketService.read.getPacketDetailByChannelId(channelId, function(err, result){
                    console.log(err || result)
                    console.log("finish get Plan")
                    callback(err, result)
                })
            },
            playInfo: ["videoInfo", function(result, callback){
                VideoService.read.getPlayInfo(result.videoInfo.id, function(err, result){
                    console.log(err || result)
                    callback(err, result)
                })
            }]
        }, function(err, results){
            console.log("123456")
            console.log(err || results);
            var videoInfo = results.videoInfo;
            var liveInfo = results.liveInfo;
            var packetplan = results.getPlan.info.usingPacket;
            var playInfo = results.playInfo
            liveInfo.events.push(time)
            console.log(packetplan.info.quality)
            //update video state
            var video_state = new VideoService.type.MemeVideoState();
            video_state.videoId = videoInfo.id;
            video_state.processState = VideoService.type.ProcessState.UPLOADING;
            video_state.videoState = VideoService.type.VideoState.INITIAL;
            video_state.adState = VideoService.type.AdState.INITIAL;
            video_state.approveState = VideoService.type.ApproveState.INITIAL;
            console.log(video_state)
            VideoService.write.updateState(video_state, function(err, result){
                console.log(err || result)
            })

            // processing file copy to convert location -> push to gearman
            function genVideoLink(time){
                var year = time.getYear()+1900;
                var month = time.getMonth()+1;
                var date = time.getDate();
                var hour = time.getHours();
                var minute = time.getMinutes();
                return year+"/"+month+"/"+date+"/" + hour + "/" +minute;
            }
            
            //if not program -> process video at the end
            if (!liveInfo.program) {
                var info = {
                    input: "/data/media/livestream/"+ playInfo.info.file,
                    output: "clip/" + genVideoLink(new Date(videoInfo.createTime)),
                    videoId: Number(videoInfo.id),
                    aliasId: videoInfo.aliasId,
                    quality: packetplan.info.quality,
                    events: liveInfo.events,
                    videoKey: playInfo.info.file
                }
                console.log(info)
                gearman.submitJob("ProcessingLive", JSON.stringify(info),false,{background:true});
            }

        })

		
    });

    req.pipe(busboy);
}