var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js");
var ScheduleAPI = require("../../../Services/LiveSchedule.js");
var path = require("path");
var fs = require("fs");
var os = require("os");
var async = require("async");
var VideoService = require("../Thrift/VideoService");
var execSync = require("child_process").execSync;
var Logger = require("../libs/Logger")
var UpdateSchedule = require("../Tasks/schedule.js")

exports = module.exports = function(req, res){

    var locals = res.locals;

    var aliasId;
    var schedule;
    var channelId = locals.channelID;
    var ownerId = locals.userID;
   

    var busboy = new Busboy({ headers: req.headers });
    var time = new Date()
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        console.log(fieldname + " --- " + val);
        if (fieldname=="aliasId" && val) aliasId = val;
        if (fieldname=="schedule" && val) schedule = val;
    });


    busboy.on('finish', function() {
        
        var curTime = (new Date()).getTime()
        if (schedule < curTime){
            Logger.info("schedule less than current time")
            res.json({error:-4,message:"request time not valid"});
            return;
        }

        var info = {
            aliasId: aliasId
        }

        LiveAPI.get(info, function(err, result){
            // request error
            if (err){
                Logger.info("Request error")
                res.json({error:-1,message:"request error"});
                return;
            }

            //service error
            result = JSON.parse(result)
            if (result.status!="000"){
                Logger.info(result)
                res.json({error:-2,message:"service error"});
                return;
            }

            var data = result.data;
            var publish = data.publish;
            console.log(data)
            if (publish !== false){
                Logger.info("Live already started")
                res.json({error:-3,message:"already start"});
                return;
            }

            var info = {
                aliasId: aliasId,
                schedule: schedule
            }
            LiveAPI.update(info, function(err, result){
                console.log(err, result)
                UpdateSchedule.update()
                res.json({error:0});
            })

        })

        
        
    });

    req.pipe(busboy);
};

