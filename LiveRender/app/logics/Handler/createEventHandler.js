var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js");
var path = require("path");
var fs = require("fs");
var os = require("os");
var async = require("async");
var VideoService = require("../Thrift/VideoService");
var execSync = require("child_process").execSync;
var Logger = require("../libs/Logger")


exports = module.exports = function(req, res){

    var locals = res.locals;

	var title = "";
	var imagePath = "";
	var channelId = locals.channelID;
    var ownerId = locals.userID;
    var types_ids = [];
    var date,minute, hour;
    var description = "";

	var busboy = new Busboy({ headers: req.headers });
    var time = new Date()
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        console.log(fieldname + " --- " + val);
        if (fieldname=="title" && val) title = val;
        if (fieldname=="description" && val) description = val;
    });

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
        console.log(fieldname)
        console.log(file)
		imagePath = path.join(os.tmpDir(), path.basename(time.getTime()+""));
		file.pipe(fs.createWriteStream(imagePath));
    });

    busboy.on('finish', function() {
    

    	if (!title || title.trim().length<=3){
            Logger.info("title null ")
    		res.json("{}");
    		return;
    	}

    	if (!channelId || isNaN(channelId)){
            Logger.info("channelId null")
    		res.json("{}");
    		return;
    	} 

    	
        var videoID = 0;

    	async.auto({
            // create video in VideoService
			create_videoservice: function(callback){
                var newLiveStream = new VideoService.type.MemeVideo();
                newLiveStream.ownerId = ownerId;
                newLiveStream.channelId = channelId;
                newLiveStream.title = title;
                newLiveStream.description = description;
                newLiveStream.createTime = time.getTime();
                VideoService.write.add(newLiveStream, function(err, result){
                    if (err){
                        callback(true,{msg: "Thrift error"});
                        return;
                    }
                    if (result.error!=0){
                        callback(true, result);
                        return;
                    }
                    callback(null, result)
                })
			},
            // get alias ID of new video
            // create PlayInfo
            get_alias: ["create_videoservice", function (results, callback) {
                videoID = results.create_videoservice.videoId;
                VideoService.read.getVideoById(videoID, function(err, result){
                    if (err){
                        callback(true,{msg: "Thrift error"});
                        return;
                    }
                    if (result.error!=0){
                        callback(true, result);
                        return;
                    }
                    callback(null, result)
                })


            }],
            create_liveservice: ["get_alias", function(results, callback){
			    var newVideoInfo = results.get_alias.video;
                console.log(newVideoInfo);
                var info = {
                    aliasId: newVideoInfo.aliasId,
                    rootAliasId: newVideoInfo.rootAliasId
                };
                LiveAPI.create(info, function(err , result){
                    res.header("Access-Control-Allow-Origin", "*");
                    if (!err) {
                        result = JSON.parse(result);

                        if (result.status == 0){
                            callback(null, result.data)
                        }
                        else{
                            callback(result.status)
                        }
                        res.json(result.data)
                    } else {
                        callback(err);
                        res.json("{}")
                    }
                })

                
            }],
			saveImage: ["get_alias", function(results, callback){
                function genVideoLink(){
                    var year = time.getYear()+1900;
                    var month = time.getMonth()+1;
                    var date = time.getDate();
                    var hour = time.getHours();
                    var minute = time.getMinutes();
                    return year+"/"+month+"/"+date+"/" + hour + "/" +minute;
                }
                var newVideoInfo = results.get_alias.video;
                // var info = {};
				if (fs.existsSync(imagePath) && fs.statSync(imagePath)["size"]!=0){
				    var newName= newVideoInfo.aliasId + "_" + (new Date()).getTime();
					var source = fs.createReadStream(imagePath);
                    var dateLink =  genVideoLink();
                    Logger.info("create folder " + "mkdir -p /data/images/thumb/clip/"+dateLink)
                    Logger.info(execSync("mkdir -p /data/images/thumb/clip/"+dateLink));
					var dest = fs.createWriteStream("/data/images/thumb/clip/"+dateLink+"/"+newName+".jpg");
					source.pipe(dest);
                    Logger.info("create image " + "/data/images/thumb/clip/"+dateLink+"/"+newName+".jpg");
					// info.aliasId = newVideoInfo.aliasId;
					// LiveAPI.update(info, function(err , result){
					// 	if (!err) {
					// 		console.log(result)
					// 	} else {
					// 		console.log(err)
					// 	}
			  //   	})
                    imagePath = dateLink+"/"+newName+".jpg";
                    callback(null, imagePath);
				} else{
                    callback(null, "");
                }

				
			}],
            finalUpdate: ["saveImage", function(results, callback){
			    Logger.info(results);

                if (results.saveImage){
                    var updateVideo = results.get_alias.video;
                    var thumbnail = results.saveImage;
                    var updateLiveStream = new VideoService.type.MemeVideo();
                    updateLiveStream.id = updateVideo.id;
                    updateLiveStream.thumbnail = thumbnail;

                    VideoService.write.update(updateLiveStream, function(err, result){
                        console.log(err || result)
                        if (err){
                            callback(true,{msg: "Thrift error"});
                            return;
                        }
                        if (result.error!=0){
                            callback(true, result);
                            return;
                        }
                        callback(null, result)
                    })
                } else {
                    callback(null)
                }
                
            }]
		}, function(err,result){
            console.log(result)
            //update_playinfo
            var playerInfo = new VideoService.type.MemeVideoPlayInfo();
            playerInfo.videoId = videoID;
            playerInfo.uploadedTime = time.getTime();
            playerInfo.file = result.create_liveservice.key;
            VideoService.write.updatePlayInfo(playerInfo, function(err, result){
                console.log(err || result)
            })

            var video_state = new VideoService.type.MemeVideoState();
            video_state.videoId = videoID;
            video_state.processState = VideoService.type.ProcessState.NOT_UPLOADED;
            video_state.videoState = VideoService.type.VideoState.PUBLIC;
            video_state.adState = VideoService.type.AdState.CHANNEL_SETTING;
            video_state.approveState = VideoService.type.ApproveState.APPROVE;
            console.log(video_state)
            VideoService.write.updateState(video_state, function(err, result){
                console.log(err || result)
            })
            
			console.log("err " +  err);
			console.log("result " + JSON.stringify(result))
		})
    	
    	
    });

    req.pipe(busboy);
};

