var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js")
var path = require("path")
var fs = require("fs")
var os = require("os")
var async = require("async")
var VideoService = require("../Thrift/VideoService");
var RealtimeService = require("../Thrift/RealtimeService");

exports = module.exports = function(req, res){

    var aliasId = "";
    var locals = {}
    res.time = Math.random()*1000;
    // console.log(res.time)
    var busboy = new Busboy({ headers: req.headers });
               
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        // console.log(fieldname + " --- " + val)
        if (fieldname=="aliasId" && val) aliasId = val;
    });


    busboy.on('finish', function() {

        if (!aliasId || aliasId=="undefined"){
            res.end("N/A")
            return;
        }

        var info = {
            aliasId: aliasId
        };
        res.header("Access-Control-Allow-Origin","*")
        async.auto({
            videoInfo: function(callback){
                VideoService.read.getVideoByAlias(aliasId, function(err, result){
                    if (err){
                        callback(true,{msg: "Thrift error"});
                        return;
                    }
                    if (result.error!=0){
                        callback(true, result);
                        return;
                    }
                    callback(null, result.video)
                })
            },
            realtime: ["videoInfo", function(results, callback){
                var videoInfo = results.videoInfo;
                RealtimeService.read.getVideo(Number(videoInfo.id), function(err, result){
                    console.log(err || result);
                    try{
                        if (err || !result || result.error!=0){
                            callback(null, 0);        
                        } else{
                            callback(null, result.data.play);    
                        }    
                    } catch(err){
                        callback(null, 0);        
                    }
                })
            }],
            liveInfo: function(callback){
                LiveAPI.get(info, function(err, result){
                    // console.log(err || result);
                    if (err) {
                        callback(err);
                        return;
                    }

                    result = JSON.parse(result);
                    if (result.status != 0) {
                        callback(result)
                        return;
                    }
                    callback(null, result.data)
                })
            },
            response: ["realtime","videoInfo","liveInfo", function(results, callback){
                var videoInfo = results.videoInfo;
                var liveInfo = results.liveInfo;
                var realtime = results.realtime;

                locals.publishTime = liveInfo.events[0] || 0,
                locals.publish = liveInfo.publish;
                locals.state = liveInfo.state;
                locals.signal = liveInfo.signal;
                locals.activity = liveInfo.activity;
                locals.realtime = realtime
                res.json(locals);
            }]
        }, function(err, results){
            // console.log(err || results);
        })
        
    });

    req.pipe(busboy);
}