var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js");
var path = require("path");
var fs = require("fs");
var os = require("os");
var async = require("async");
var VideoService = require("../Thrift/VideoService");
var execSync = require("child_process").execSync;
var Logger = require("../libs/Logger")


exports = module.exports = function(req, res){

    var locals = res.locals;

	var title = "";
	var imagePath = "";
	var channelId = locals.channelID;
    var ownerId = locals.userID;
    var types_ids = [];
    var date,minute, hour;
    var description = "";
    var aliasId = "";

	var busboy = new Busboy({ headers: req.headers });
    var time = new Date()
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        console.log(fieldname + " --- " + val);
        if (fieldname=="title" && val) title = val;
        if (fieldname=="description" && val) description = val;
        if (fieldname=="aliasId" && val) aliasId = val;
    });

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
        console.log(fieldname)
        console.log(file)
		imagePath = path.join(os.tmpDir(), path.basename(time.getTime()+""));
		file.pipe(fs.createWriteStream(imagePath));
    });

    busboy.on('finish', function() {
    
    	if (!title || title.trim().length<=3){
            Logger.info("title null ")
    		res.json("{}");
    		return;
    	}

    	if (!channelId || isNaN(channelId)){
            Logger.info("channelId null")
    		res.json("{}");
    		return;
    	} 

        if (!aliasId || aliasId.trim().length<=3){
            Logger.info("aliasId null ")
            res.json("{}");
            return;
        }

        var videoID = 0;

    	async.auto({
            // create video in VideoService
			get_videoservice: function(callback){
                VideoService.read.getVideoByAlias(aliasId, function(err, result){
                    if (err){
                        callback(true,{msg: "Thrift error"});
                        return;
                    }
                    if (result.error!=0){
                        callback(true, result);
                        return;
                    }
                    callback(null, result)
                })
			},
            get_liveservice: function(callback){
                var info = {
                    aliasId: aliasId,
                };
                LiveAPI.get(info, function(err , result){
                    if (!err) {
                        result = JSON.parse(result);
                        if (result.status == 0){
                            callback(null, result.data)
                        }
                        else{
                            callback(result.status)
                        }
                    } else {
                        callback(err);
                    }
                })   
            },
			saveImage: ["get_liveservice","get_videoservice", function(results, callback){
                function genVideoLink(){
                    var year = time.getYear()+1900;
                    var month = time.getMonth()+1;
                    var date = time.getDate();
                    var hour = time.getHours();
                    var minute = time.getMinutes();
                    return year+"/"+month+"/"+date+"/" + hour + "/" +minute;
                }
                var newVideoInfo = results.get_videoservice.video;
                // var info = {};

				if (fs.existsSync(imagePath) && fs.statSync(imagePath)["size"]!=0){
				    var newName= newVideoInfo.aliasId + "_" + (new Date()).getTime();
					var source = fs.createReadStream(imagePath);
                    var dateLink =  genVideoLink();
                    Logger.info("create folder " + "mkdir -p /data/images/thumb/clip/"+dateLink)
                    Logger.info(execSync("mkdir -p /data/images/thumb/clip/"+dateLink));
					var dest = fs.createWriteStream("/data/images/thumb/clip/"+dateLink+"/"+newName+".jpg");
					source.pipe(dest);
                    Logger.info("create image " + "/data/images/thumb/clip/"+dateLink+"/"+newName+".jpg");
					
     //                info.aliasId = newVideoInfo.aliasId;
					// LiveAPI.update(info, function(err , result){
					// 	if (!err) {
					// 		console.log(result)
					// 	} else {
					// 		console.log(err)
					// 	}
			  //   	})
                    imagePath = dateLink+"/"+newName+".jpg";
                    callback(null, imagePath);
				} else{
                    callback(null, "");
                }

			}],
            finalUpdate: ["get_videoservice", "saveImage", function(results, callback){
			    Logger.info(results);

                if (results.saveImage){
                    var updateVideo = results.get_videoservice.video;
                    var thumbnail = results.saveImage;
                    var updateLiveStream = new VideoService.type.MemeVideo();
                    updateLiveStream.id = updateVideo.id;
                    updateLiveStream.thumbnail = thumbnail;

                    VideoService.write.update(updateLiveStream, function(err, result){
                        console.log(err || result)
                        if (err){
                            callback(true,{msg: "Thrift error"});
                            return;
                        }
                        if (result.error!=0){
                            callback(true, result);
                            return;
                        }
                        callback(null, result)
                    })
                } else {
                    callback(null)
                }
                
            }]
		}, function(err,results){
            if (err){
                console.log(err)
                res.json({})
                return
            }


            console.log(results)

            var video = results.get_videoservice.video
            var updateLiveStream = new VideoService.type.MemeVideo();
            updateLiveStream.title = title
            updateLiveStream.description = description
            updateLiveStream.id = Number(video.id)
            console.log(updateLiveStream)
            VideoService.write.update(updateLiveStream, function(err, result){
                console.log(err || result)
            })
            
			console.log("err " +  err);
			console.log("result " + JSON.stringify(results))
            res.json({});
		})
    	
    	
    });

    req.pipe(busboy);
};

