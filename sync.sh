#server1="103.27.237.224"
server1="118.70.206.227"

live_service="7560"
live_render="7561"
live_event="7559"
live_status="7558"


mode="development"
srcFolder=${1%/}
task=$2
desFolder="/opt/app"

if [[ "$srcFolder" == LiveService ]] ;then
	server=( "$server1" )
	port=$live_service
	user="zdeploy"
elif [[ "$srcFolder" == LiveRender ]] ;then
	server=( "$server1" )
	port=$live_render
	user="zdeploy"
elif [[ "$srcFolder" == LiveEventService ]] ;then
	server=( "$server1" )
	port=$live_event
	user="zdeploy"
elif [[ "$srcFolder" == LiveStatus ]] ;then
	server=( "$server1" )
	port=$live_status
	user="root"
elif [[ "$srcFolder" == LiveProcessing ]] ;then
	server=( "$server1" )
	port=""
	user="root"
else
	echo "Please register project folder or specify in command line "
	exit;
fi

init(){
	copy ${server[@]}
	echo "Init Project ...."
	for i in ${server[@]}
	do
		ssh $user@$i "cd ${desFolder}/$srcFolder && npm install"
	done
}

copy(){
	echo "Copy Files ...."
	for i in ${server[@]}
	do
		rsync -avz --exclude $srcFolder/node_modules $srcFolder $user@$i:$desFolder
	done
	
}

restart(){	
	echo "Restart service ...."
	for i in ${server[@]}
	do
		ssh $user@$i "cd ${desFolder}/$srcFolder && NODE_ENV=${mode} ./run.sh ${port}"
	done
	
}

log(){
	echo "Reading Log File ...."
	for i in ${server[@]}
	do
		ssh $user@$i tail -f -n 100 ${desFolder}/$srcFolder/main.log
	done
}

if [ "$task" = "init" ] ;then
	init 
	restart 
elif [ "$task" = "copy" ] ;then
	copy 
	restart 
	log
elif [ "$task" = "restart" ] ;then
	restart 
elif [ "$task" = "log" ] ;then
	log 
else
	echo "[ERROR] Usage: ./sync Project [init |copy|restart|log]"
fi
