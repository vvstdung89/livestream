var Event = {}
var async = require("async")
var LiveStream = require("../../models/LiveStream");
var LiveService = require("../../../Services/LiveService");
var Logger = require("../libs/Logger")
exports = module.exports = Event;

function genKey(){
	var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 12; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}



Event.createLiveEvent = function(title, callback){

	function checkKey(callback){
		var key = genKey();
		LiveStream.findOne({
			key: key
		}).exec(function(err, result){
			if (err) {
				callback(err)
			} else {
				if (result){
					checkKey(callback)
				} else {
					callback(null, key)
				}
			}
		})
	}
	
	checkKey(function(err, result){
		if (!err){
			var key = result;
			var live_event = new LiveStream();
			live_event.key = key
			live_event.state = "new"
			live_event.title = title
			live_event.channelID = 1
			live_event.status = [{
				time: (new Date()).getTime(),
				action: "create"
			}]
			live_event.save(function(err){
				Logger.info(err || live_event)
				callback(null, live_event)
			})
		} else {
			Logger.info(err)
			callback(err)
		}
	})
}

Event.endLiveEvent = function(key, callback){
	LiveStream.findOne({
		key: key
	}).exec(function(err, result){
		if (err) {
			callback(err)
		} else {
			if (result){
				LiveStream.update({
					key:key
				}, {
					state: "end"
				}, function(err, result){
					if (err){
						callback(err)
					} else
						callback(null)
				})
			} else {
				callback("Not Exist")
			}
		}
	})
	
}

Event.callback = function(req, res){
	var info = req.body;
	// Logger.info(info);

	//Connect callback
	if (info.call=="connect"){
		res.end()
	} 
	//Publish callback
	else if (info.call=="publish"){
		Logger.info("signal -> true " + JSON.stringify(info));
		var info = {
			key: info.name
		}
		LiveService.get(info, function(err, result){
			console.log(err||result)
			if (!err){
				result = JSON.parse(result);
				if (result.status=="000"){
					console.log(result.data)
					if (result.data.state!="end"){
						res.end()
						info.state = "started"
						info.signal = true
						info.activity = [{
							time: (new Date()).getTime(),
							name: 4
						}]
						LiveService.update(info, function(err, result){
						})
						return;
					}
				}
			}
			res.status(404);
		});
	} 
	//Done(=paused) callback
	else if (info.call=="publish_done"){
		Logger.info("signal -> false " + JSON.stringify(info));
		var info = {
			key: info.name
		}
		LiveService.get(info, function(err, result){
			if (!err){
				result = JSON.parse(result);
				if (result.status=="000"){
					res.end()

					info.signal = false
					info.activity = [{
						time: (new Date()).getTime(),
						name: 5
					}]
					LiveService.update(info, function(err, result){
					})
					return;
				}
			}
			res.status(404);
		});
	}
	//Update(=paused) callback
	else if (info.call=="update_publish"){
		Logger.info("update_publish " + JSON.stringify(info));
		var info = {
			key: info.name
		}
		if (info.time > 60*60*2){
			LiveService.get(info, function(err, result){
				if (!err){
					result = JSON.parse(result);
					if (result.status=="000"){
						res.end()
						info.state = "end";
						info.activity = [{
							time: (new Date()).getTime(),
							name: 8
						}]
						LiveService.update(info, function(err, result){
						})
						return;
					}
				}
				res.status(404);
			});
			res.status(404);
		} else {
			LiveService.get(info, function(err, result){
				if (!err){
					result = JSON.parse(result);
					if (result.status=="000"){
						info.signal = true
						LiveService.update(info, function(err, result){
						})
						res.end()
						return;
					}
				}
				res.status(404);
			});
		}

	} else if (info.call=="update_play" ){
		res.end()
	} else if (info.call=="done" ){
		res.end()
	}
	//Other
	else {
		Logger.info("not allow")
		res.status(404)
	}
}