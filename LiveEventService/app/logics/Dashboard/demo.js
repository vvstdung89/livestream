var Logger = require("../libs/Logger")
var ERROR = require("../libs/ErrorCode")
var Common = require("../libs/Common")
var Event = require("../EventHandler/handler")
var Busboy = require("busboy");
var LiveStream = require("../../models/LiveStream")

exports = module.exports = function(req, res){
	if (req.method=="GET"){
		res.render("demo")
	}
	if (req.method=="POST"){
		var title = "";
		var action = "";
		var key = "";
		var busboy = new Busboy({ headers: req.headers });
               
        busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
            if (fieldname=="title" && val) title = val;
            if (fieldname=="action" && val) action = val;
            if (fieldname=="key" && val) key = val;
            // console.log(fieldname + " --- " + val)
        });

        busboy.on('finish', function() {
        	if (action == "create")
	             Event.createLiveEvent(title, function(err, result){
	             	if (err){
	             		console.log(err)
	             		res.end();
	             	} else {
	             		res.json(result);
	             	}
	             })

	         if (action == "end")
	             Event.endLiveEvent(key, function(err, result){
	             	if (err){
	             		console.log(err)
	             		res.end();
	             	} else {
	             		res.end();
	             	}
	             })
	         if (action == "status") {
				LiveStream.findOne({
					key: key
				}).exec(function(err, result){
					if (err ||  !result){
						res.json("")
					} else {
						res.json(result)
					}
				})
	         }
	             
        });
        req.pipe(busboy);
	         
	}
}