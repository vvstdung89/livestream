exports = module.exports = {
	SUCCESS: 0,
	ERROR: -1,
	NOT_FOUNT: -2,
	MISSING_INFO: -3,
	DB_ERROR: -4,
	NOT_EXIST: -5,
}