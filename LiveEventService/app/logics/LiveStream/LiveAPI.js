var Logger = require("../libs/Logger.js")
var LiveStream = require("../../models/LiveStream.js")
var STATUS = require("../libs/ErrorCode.js")
var LiveStream = function(){};

LiveStream.prototype.create = function(req,res){
	var newLiveInfo = req.body;
	Logger.info("Receive create live task: " + JSON.stringify(newLiveInfo))

	function checkKey(callback){
		var key = genKey();
		LiveStream.findOne({
			key: key
		}).exec(function(err, result){
			if (err) {
				callback(err)
			} else {
				if (result){
					checkKey(callback)
				} else {
					callback(null, key)
				}
			}
		})
	}

	checkKey(function(err, result){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR " + err.message;
			res.json(httpReply);
			Logger.info("Create live error " + err.message)
			return;
		}

		var newLiveObj = new LiveStream();
		for (var i in newLiveInfo){
			newLiveObj[i] = newLiveInfo[i];
		}
		newLiveObj.key =  result
		var httpReply = {};
		newLiveObj.time_create = (new Date()).getTime()
		newLiveObj.save(function(err){
			if (!err){
				httpReply.status = STATUS.SUCCESS;
				httpReply.data = newLiveObj;
				res.json(httpReply);
				Logger.info("Create live successfully " + JSON.stringify(newLiveObj))
			} else{
				httpReply.status = STATUS.DB_ERROR;
				httpReply.data = "DB_ERROR " + err.message;
				res.json(httpReply);
				Logger.info("Create live error " + err.message)
			}
		})

	})
	
}

LiveStream.prototype.delete = function(req,res){
	var deleteLiveInfo = req.body;
	Logger.info("Receive delete live task: " + JSON.stringify(deleteLiveInfo))
	var httpReply = {};

	//check input
	if (!deleteLiveInfo.email && !deleteLiveInfo.id){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: email or id need to specify";
		res.json(httpReply);
		Logger.info("Remove live error MISSING_INFO")
		return;
	}

	//get live
	LiveStream.findOne({
		$or: [{_id: deleteLiveInfo._id},{email: deleteLiveInfo.email}]
	}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Remove live error DB_ERROR")
			return;
		}

		if (!returnObj) {
			httpReply.status = STATUS.NOT_EXIST;
			httpReply.data = "NOT_EXIST: live not exist";
			res.json(httpReply);
			Logger.info("Remove live error NOT_EXIST")
			return;
		}
		//delete
		LiveStream.find({_id:returnObj._id}).remove().exec(function(err, deleteResult){
			if (err){
				httpReply.status = STATUS.DB_ERROR;
				httpReply.data = "DB_ERROR: " + err.message;
				res.json(httpReply);
				Logger.info("Remove live error DB_ERROR")
				return;
			}
			httpReply.status = STATUS.SUCCESS;
			httpReply.data = "SUCCESS: remove successfully";
			res.json(httpReply);
			Logger.info("Remove live successfully")
		})
	})
}

LiveStream.prototype.update = function(req,res){
	var updateLiveInfo = req.body;
	Logger.info("Receive update live task: " + JSON.stringify(updateLiveInfo))
	var httpReply = {};

	//check input
	if ( !updateLiveInfo.key){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: key need to specify";
		res.json(httpReply);
		Logger.info("Update live error MISSING_INFO")
		return;
	}

	//get live
	LiveStream.findOne({
		key: updateLiveInfo.key
	}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Update live error DB_ERROR")
			return;
		}

		if (!returnObj) {
			httpReply.status = STATUS.NOT_EXIST;
			httpReply.data = "NOT_EXIST: live not exist";
			res.json(httpReply);
			Logger.info("Update live error NOT_EXIST")
			return;
		}
		//delete
		Logger.info("Updating: " + JSON.stringify(updateLiveInfo))
		LiveStream.update({
			_id: returnObj._id
		}, updateLiveInfo, function(err, updateResult){
			if (err){
				httpReply.status = STATUS.DB_ERROR;
				httpReply.data = "DB_ERROR: " + err.message;
				res.json(httpReply);
				Logger.info("Update live error DB_ERROR")
				return;
			}
			httpReply.status = STATUS.SUCCESS;
			httpReply.data = "SUCCESS: update successfully " + JSON.stringify(updateResult);
			res.json(httpReply);
			Logger.info("Update live successfully")
		})
	})
}

LiveStream.prototype.get = function(req,res){
	var getLiveInfo = req.body;
	Logger.info("Receive get task: " + JSON.stringify(getLiveInfo))
	var httpReply = {};

	//check input
	if ( !getLiveInfo.key){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: key need to specify";
		res.json(httpReply);
		Logger.info("Get live error MISSING_INFO")
		return;
	}

	//get live
	LiveStream.findOne({
		key: getLiveInfo.key
	}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Get live error DB_ERROR")
			return;
		}

		if (!returnObj) {
			httpReply.status = STATUS.NOT_EXIST;
			httpReply.data = "NOT_EXIST: live not exist";
			res.json(httpReply);
			Logger.info("Get live error NOT_EXIST")
			return;
		}
		//delete
		httpReply.status = STATUS.SUCCESS;
		httpReply.data = returnObj;
		res.json(httpReply);
		Logger.info("Get live successfully")
	})
}

LiveStream.prototype.list = function(req,res){
	var listLiveInfo = req.body;
	Logger.info("Receive list live task: " + JSON.stringify(listLiveInfo))
	var httpReply = {};

	//check input
	if (!listLiveInfo.from  || !listLiveInfo.getSize){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: from and getSize need to specify";
		res.json(httpReply);
		Logger.info("Get list live error MISSING_INFO")
		return;
	}

	//get live
	LiveStream.find().skip(Number(listLiveInfo.from)).limit(Number(listLiveInfo.getSize))
	.exec(function(err, returnObjs){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Get list live error DB_ERROR")
			return;
		}
		httpReply.status = STATUS.SUCCESS;
		httpReply.data = returnObjs;
		res.json(httpReply);
		Logger.info("Get list live successfully")
	})
}

LiveStream.prototype.search = function(req,res){
	
}

exports = module.exports = new LiveStream();