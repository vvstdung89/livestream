var mongoose     = require('mongoose');
var config = require("../../Config.js")
var Schema       = mongoose.Schema;


var LiveStream  = new Schema({
	channelID: Number,
	state: String, // new | streamming | paused | end
	status: [],
	title: String,
	key: {
		type: String,
		index: true,
		unique: true
	}
});

// LiveStream.set('autoIndex', true);
// LiveStream.index({ownerID: 1, time: -1}, {unique: true});

exports = module.exports = mongoose.model('LiveStream', LiveStream);

