

exports = module.exports = function (app, router){

	//root service
	app.use("/", router);

	
	// router.post('/callback', require('./app/logics/callback.js'));
	// router.get('/callback', require('./app/logics/callback.js'));
	// router.get("/demo", require("./app/logics/Dashboard/demo.js"))
	// router.post("/demo", require("./app/logics/Dashboard/demo.js"))
    //
	// router.get("/demo/:key", require("./app/logics/Embed/player.js"))

	router.post("/LiveEventService/callback", require("./app/logics/EventHandler/handler.js").callback)

	app.get('*', function(req, res) {
	    res.status(404)
  		res.send('Not found page.');
	});
}
