var request = require('request');
var service_name = "LiveService";

exports = module.exports = {

	//info must have name, owner and upload_type must specify must be specified
	create: function(info, callback){
		var caller = "create";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},

	delete: function(info, callback){
		var caller = "delete";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},


	//key as only parameter
	get: function(info, callback){
		var caller = "get";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},

	

	//info must have pos and range
	list: function(info, callback){
		var caller = "list";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},

	

	//info must provide key and at least one update field size, type, associated_account, stream1, stream2, resolution, description, status)
	update: function(info, callback){
		var caller = "update";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},



};


