var fs = require("fs")
var Logger = function(){

}

var logger = new Logger();

var path = require("path")
Object.defineProperty(global, '__stack', {
get: function() {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
get: function() {
        return __stack[2].getLineNumber();
    }
});

Object.defineProperty(global, '__function', {
get: function() {
        return __stack[2].getFileName();
    }
});

Logger.prototype.debug = function(str){
	var date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
	if (typeof str == "object" ) {
		console.log(date + " [DEBUG:" +path.basename(__function) + ":" + __line + "]: ");
		console.log(str)
	} else
	console.log(date + " [DEBUG:" +path.basename(__function) + ":" + __line + "]: " + str);
}

Logger.prototype.info = function(str){
	var date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
	if (typeof str == "object" ) {
		console.log(date + " [INFO:" +path.basename(__function) + ":" + __line + "]: ");
		console.log(str)
	} else
	console.log(date + " [INFO:"+path.basename(__function) + ":" + __line + "] - " + str);
}

Logger.prototype.error = function(str){
	var date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
	if (typeof str == "object" ) {
		console.log(date + " [ERROR:" +path.basename(__function) + ":" + __line + "]: ");
		console.log(str)
	} else
	console.log(date + " [ERROR:"+path.basename(__function) + ":" + __line + "] - " + str);
}

Logger.prototype.request = function(str){
	fs.appendFile('request.log', str +"\n" , function (err) {
		if (err){
			logger.error("Cannot write to file -> critical error");
			process.exit(1);
		}
	});
}

exports = module.exports = logger;
