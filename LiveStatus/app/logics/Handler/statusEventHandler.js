var Busboy = require("busboy");
var LiveAPI = require("../../../Services/LiveStream.js")
var RealtimeService = require("../Thrift/RealtimeService.js")
var path = require("path")
var fs = require("fs")
var os = require("os")
var async = require("async")
var VideoService = require("../Thrift/VideoService");
var Logger = require("../libs/Logger")

exports = module.exports = function(req, res){
    console.log(req.body)
    var aliasId = req.body.aliasId;
    var preview = req.body.preview;

    var locals = {}

    if (!aliasId){
        res.end("N/A")
        return;
    }

    var info = {
        aliasId: aliasId
    };
    console.log(aliasId)
    res.header("Access-Control-Allow-Origin","*")
    async.auto({
        videoInfo: function(callback){
            VideoService.read.getVideoByAlias(aliasId, function(err, result){
                if (err){
                    callback(true,{msg: "Thrift error"});
                    return;
                }
                if (result.error!=0){
                    callback(true, result);
                    return;
                }
                callback(null, result.video)
            })
        },
        realtime: ["videoInfo", function(results, callback){
            var videoInfo = results.videoInfo;
            Logger.info(videoInfo)
            RealtimeService.read.getVideo(Number(videoInfo.id), function(err, result){
                console.log(err || result);
                try{
                    if (err || !result || result.error!=0){
                        callback(null, 0);        
                    } else{
                        callback(null, result.data.play);    
                    }    
                } catch(err){
                    callback(null, 0);        
                }
            })
        }],
        liveInfo: function(callback){
            LiveAPI.get(info, function(err, result){
                console.log(err || result);
                if (err) {
                    callback(err);
                    return;
                }
                result = JSON.parse(result);
                if (result.status != 0) {
                    callback(result)
                    return;
                }
                callback(null, result.data)
            })
        },
        response: ["realtime","liveInfo", function(results, callback){
            var realtime = results.realtime;
            var liveInfo = results.liveInfo;
            locals.state = liveInfo.state
            locals.publish = liveInfo.publish || false;
            if (realtime){
                locals.realtime =  realtime
            } else
                locals.realtime =  0
            if (locals.state=="end"){
                //TODO: get clip
            } else {
                if (preview==="true" && req.headers.referer.indexOf("mecloud.vn")>-1){
                    console.log("preview mode /data/media/livestream/"+liveInfo.key+"/index.m3u8"+ fs.existsSync("/data/media/livestream/"+liveInfo.key+"/index.m3u8"))

                    if (locals.state=="started" && fs.existsSync("/data/media/livestream/"+liveInfo.key+"/index.m3u8")){
                        locals.src=[{
                            src: "http://stream-live.mecloud.vn/"+liveInfo.key+"/index.m3u8",
                            type:  "application/x-mpegURL",
                            label: "auto"
                        }];
                        locals.signal = liveInfo.signal;
                    }
                } else if (preview!=="true"){
                    console.log("non preview mode")
                    if (locals.state=="started" && locals.publish==true && fs.existsSync("/data/media/livestream/"+liveInfo.key+"/index.m3u8")){
                        locals.src=[{
                            src: "http://stream-live.mecloud.vn/"+liveInfo.key+"/index.m3u8",
                            type:  "application/x-mpegURL",
                            label: "auto"
                        }];
                        locals.signal = liveInfo.signal;
                    }
                }
            }



            res.json(locals);
        }]
    }, function(err, results){
        console.log(err || results);
    })

}