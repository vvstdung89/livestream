/**
 * Created by david89 on 9/14/2016.
 */
var thrift = require('thrift');
var RealtimeClient_READ = require("../../../Services/RealtimeService/AnalyticsRealtime");

var transport = require('thrift/lib/nodejs/lib/thrift/framed_transport.js');
var protocol  = require('thrift/lib/nodejs/lib/thrift/binary_protocol.js');

var RealtimeRead = thrift.createClient(RealtimeClient_READ, thrift.createConnection("mecloud.vn",9223, {
    transport : transport,
    protocol : protocol
}));


exports = module.exports = {
    read: RealtimeRead,
    type: require("../../../Services/RealtimeService/memecloud.analytics-realtime_types")
};
