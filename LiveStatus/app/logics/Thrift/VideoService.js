var thrift = require('thrift');
var VideoService_READ = require("../../../Services/VideoService/MemeVideo_Read.js")
var VideoService_WRITE = require("../../../Services/VideoService/MemeVideo_Write.js")
var VideoServiceType = require("../../../Services/VideoService/memecloud.video.v3_types");

var transport = require('thrift/lib/nodejs/lib/thrift/framed_transport.js');
var protocol  = require('thrift/lib/nodejs/lib/thrift/binary_protocol.js');

// var connection = thrift.createConnection("dev.mecloud.vn",9097, {
//         transport : transport,
//         protocol : protocol
// });

var readClient = thrift.createClient(VideoService_READ, thrift.createConnection("mecloud.vn",9097, {
        transport : transport,
        protocol : protocol
}));
var writeClient = thrift.createClient(VideoService_WRITE, thrift.createConnection("mecloud.vn",9299, {
        transport : transport,
        protocol : protocol
}));

// connection.on('error', function(err) {
//   console.error(err);
// });

// connection.on('close', function(err) {
//   var stack = new Error().stack
//   console.log( stack )
// });

// connection.on('connect', function(err, data) {
// 	console.log(err||data)
// });

exports = module.exports = {
	read: readClient,
	write: writeClient,
    type: VideoServiceType
}