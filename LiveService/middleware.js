var async = require('async');
exports = module.exports = function (app, router, redis_client){



	router.use(function(req, res, next) {
		res.locals = {};
	    res.redis_client = redis_client;
	    res.app = app;
	    next(); 
	});
};
