

exports = module.exports = function (app, router){

	//root service
	app.use("/", router);
	router.post('/LiveService/create', require('./app/logics/LiveStream/LiveAPI.js').create);
	router.post('/LiveService/get', require('./app/logics/LiveStream/LiveAPI.js').get);
	router.post('/LiveService/update', require('./app/logics/LiveStream/LiveAPI.js').update);
	router.post('/LiveService/getNextSchedule', require('./app/logics/LiveStream/LiveAPI.js').getNextSchedule);

	app.get('*', function(req, res) {
	    res.status(404)
  		res.send('Not found page.');
	});
}
