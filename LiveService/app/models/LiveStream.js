var mongoose     = require('mongoose');
var config = require("../../Config.js")
var Schema       = mongoose.Schema;

var LiveStream  = new Schema({
	aliasId: String,
	rootAliasId: String,
	key: String, //md5hash of aliasId and 'secretkey'
	signal: {
		type: Boolean,
		default: false
	},
	state: {
		type: String,
		default: "new"
	}, //new || started || end
	publish: {
		type: Boolean,
		default: false
	},
	activity: [],
	events: [Number],
	program: {
		type: Boolean,
		default: false
	},
	schedule: {
		type: Number,
		index: true,
	}
});

// LiveStream.set('autoIndex', true);

exports = module.exports = mongoose.model('LiveStream', LiveStream);

