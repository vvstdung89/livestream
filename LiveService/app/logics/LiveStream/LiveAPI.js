var Logger = require("../libs/Logger.js");
var LiveModel = require("../../models/LiveStream.js");
var STATUS = require("../libs/ErrorCode.js");
var LiveStream = function(){};

LiveStream.prototype.create = function(req,res){
	var newLiveInfo = req.body;
	Logger.info("Receive create live task: " + JSON.stringify(newLiveInfo));

	var newLiveObj = new LiveModel();
	for (var i in newLiveInfo){
		newLiveObj[i] = newLiveInfo[i];
	}
	newLiveObj.aliasId =  newLiveInfo.aliasId;
	
	newLiveObj.activity = [{
		time: (new Date()).getTime(),
		name: 1,
	}]

	newLiveObj.key = require('crypto').createHash('md5').update(newLiveObj.aliasId+"secretkey").digest('hex')
	var httpReply = {};

	newLiveObj.save(function(err){
		if (!err){
			httpReply.status = STATUS.SUCCESS;
			httpReply.data = newLiveObj;
			res.json(httpReply);
			Logger.info("Create live successfully " + JSON.stringify(newLiveObj))
		} else{
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR " + err.message;
			res.json(httpReply);
	  		Logger.info("Create live error " + err.message)
		}
	})

	
};

LiveStream.prototype.delete = function(req,res){
	var deleteLiveInfo = req.body;
	Logger.info("Receive delete live task: " + JSON.stringify(deleteLiveInfo));
	var httpReply = {};

	//check input
	if (!deleteLiveInfo.id && !deleteLiveInfo.aliasId){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: aliasId or id need to specify";
		res.json(httpReply);
		Logger.info("Remove live error MISSING_INFO");
		return;
	}

	//get live
	LiveModel.findOne({
		$or: [{_id: deleteLiveInfo._id},{aliasId: deleteLiveInfo.aliasId}]
	}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Remove live error DB_ERROR");
			return;
		}

		if (!returnObj) {
			httpReply.status = STATUS.NOT_EXIST;
			httpReply.data = "NOT_EXIST: live not exist";
			res.json(httpReply);
			Logger.info("Remove live error NOT_EXIST");
			return;
		}
		//delete
		LiveModel.find({_id:returnObj._id}).remove().exec(function(err, deleteResult){
			if (err){
				httpReply.status = STATUS.DB_ERROR;
				httpReply.data = "DB_ERROR: " + err.message;
				res.json(httpReply);
				Logger.info("Remove live error DB_ERROR");
				return;
			}
			httpReply.status = STATUS.SUCCESS;
			httpReply.data = "SUCCESS: remove successfully";
			res.json(httpReply);
			Logger.info("Remove live successfully")
		})
	})
};

LiveStream.prototype.update = function(req,res){
	var updateLiveInfo = req.body;
	Logger.info("Receive update live task: " + JSON.stringify(updateLiveInfo));
	var httpReply = {};

	//check input
	if ( !updateLiveInfo.aliasId && !updateLiveInfo.key){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: key need to specify";
		res.json(httpReply);
		Logger.info("Update live error MISSING_INFO");
		return;
	}

	//get live
	LiveModel.findOne({
		$or: [{key:updateLiveInfo.key},{aliasId:updateLiveInfo.aliasId}]
	}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Update live error DB_ERROR");
			return;
		}

		if (!returnObj) {
			httpReply.status = STATUS.NOT_EXIST;
			httpReply.data = "NOT_EXIST: live not exist";
			res.json(httpReply);
			Logger.info("Update live error NOT_EXIST");
			return;
		}
		//delete
		Logger.info("Updating: " + JSON.stringify(updateLiveInfo));
		if (updateLiveInfo.events){
			updateLiveInfo.events.push.apply(updateLiveInfo.events,returnObj.events)
			updateLiveInfo.events.sort()	
		}
		
		if (updateLiveInfo.activity){
			updateLiveInfo.activity.push.apply(updateLiveInfo.activity,returnObj.activity)
		}

		LiveModel.update({
			_id: returnObj._id
		}, updateLiveInfo, function(err, updateResult){
			if (err){
				httpReply.status = STATUS.DB_ERROR;
				httpReply.data = "DB_ERROR: " + err.message;
				res.json(httpReply);
				Logger.info("Update live error DB_ERROR");
				return;
			}
			httpReply.status = STATUS.SUCCESS;
			httpReply.data = "SUCCESS: update successfully " + JSON.stringify(updateResult);
			res.json(httpReply);
			Logger.info("Update live successfully")
		})
	})
};

LiveStream.prototype.get = function(req,res){
	var getLiveInfo = req.body;
	Logger.info("Receive get task: " + JSON.stringify(getLiveInfo));
	var httpReply = {};

	//check input
	if (!getLiveInfo.key && !getLiveInfo.aliasId){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: aliasId or key need to specify";
		res.json(httpReply);
		Logger.info("Get live error MISSING_INFO");
		return;
	}

	//get live
	LiveModel.findOne({
		$or: [{key:getLiveInfo.key},{aliasId:getLiveInfo.aliasId}]
	}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Get live error DB_ERROR");
			return;
		}

		if (!returnObj) {
			httpReply.status = STATUS.NOT_EXIST;
			httpReply.data = "NOT_EXIST: live not exist";
			res.json(httpReply);
			Logger.info("Get live error NOT_EXIST");
			return;
		}
		//delete
		console.log(returnObj)
		httpReply.status = STATUS.SUCCESS;
		httpReply.data = returnObj;
		res.json(httpReply);
		Logger.info("Get live successfully")
	})
};

LiveStream.prototype.list = function(req,res){
	var listLiveInfo = req.body;
	Logger.info("Receive list live task: " + JSON.stringify(listLiveInfo));
	var httpReply = {};

	//check input
	if (!listLiveInfo.from  || !listLiveInfo.getSize){
		httpReply.status = STATUS.MISSING_INFO;
		httpReply.data = "MISSING_INFO: from and getSize need to specify";
		res.json(httpReply);
		Logger.info("Get list live error MISSING_INFO");
		return;
	}

	//get live
	LiveModel.find().skip(Number(listLiveInfo.from)).limit(Number(listLiveInfo.getSize))
	.exec(function(err, returnObjs){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Get list live error DB_ERROR");
			return;
		}
		httpReply.status = STATUS.SUCCESS;
		httpReply.data = returnObjs;
		res.json(httpReply);
		Logger.info("Get list live successfully")
	})
};

LiveStream.prototype.getNextSchedule = function(req,res){
	var httpReply = {};
	LiveModel.find({
		schedule: {
			$lte: (new Date()).getTime() + 15*60*1000,		// 15 min get
			$gte: (new Date()).getTime() - 24*60*60*1000, 	// 24 hours backup
		},
		publish: false
	}).sort({schedule:1}).exec(function(err, returnObj){
		if (err){
			httpReply.status = STATUS.DB_ERROR;
			httpReply.data = "DB_ERROR: " + err.message;
			res.json(httpReply);
			Logger.info("Get live error DB_ERROR");
			return;
		}

		//delete
		console.log(returnObj)
		httpReply.status = STATUS.SUCCESS;
		httpReply.data = returnObj.length>=1 ? returnObj[0] : "";
		res.json(httpReply);
		Logger.info("Get live successfully")
	})	
};

exports = module.exports = new LiveStream();