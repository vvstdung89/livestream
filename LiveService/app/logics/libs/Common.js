var crypto = require('crypto');
var shasum = crypto.createHash('sha1');
var execSync = require('child_process').execSync;
var exec = require('child_process').exec;
var ip_hash  = "";
var ip = "";
var fs = require('fs');
var uuid = require('node-uuid');

exports = module.exports = {

	getUID: function(){
		return ip_hash + "-"+ uuid.v1();
	},

	getIP: function(){
		if (!ip){
			var isWin = /^win/.test(process.platform);

			// Notice: old nodejs dont have execSync 
			if (isWin){
				var reply = execSync('nslookup myip.opendns.com resolver1.opendns.com').toString();
				var replyArr = reply.split("Address: ");
				var code = replyArr[replyArr.length-1].trim();
			} else {
				var code = execSync('dig +short myip.opendns.com @resolver1.opendns.com').toString();
			}

			if (code){
				ip = code.toString().replace(/\n/g,"");
				shasum.update(ip.replace(/\./g,""));
				ip_hash = shasum.digest('hex');
				console.log("Public IP: " + ip);
				return ip;
			} else {
				process.exit()
			}
		} else {
			return ip;
		}
	},

	parseCookies: function(request) {
	    var list = {},
	        rc = request.headers.cookie;
	    rc && rc.split(';').forEach(function( cookie ) {
	        var parts = cookie.split('=');
	        list[parts.shift().trim()] = decodeURI(parts.join('='));
	    });
	    return list;
	},
	
	getKey: function(fullname){
		return latenize(fullname).replace(/[^a-z0-9]/gi,'-').toLowerCase()
	},

}
