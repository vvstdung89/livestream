var service_name = "LiveService"
exports = module.exports = {

	//START MODIFY
	version: "1.00",

	mode: process.env.NODE_ENV,

	ip:null, 

	rest_port: 0,

	service_name: service_name,

	redis_config: {
		enable: false,
		host: "127.0.0.1",
		port: "6379"
	},

	mongo_config: {
		enable: true,
		location: 'mongodb://127.0.0.1:27022/LiveStream-v1'
	},

	//remote the item in service_usage if you dont use
	service_usage : {
	},

	//END MODIFY
	setRESTPort : function(port){
		exports.rest_port = port;
	},
	setService: function(name,data){
		exports.service_usage[name] = data;
	},
	setSelfPath: function(path){
		exports.service_path = path;
	},
	getService: function(name){
		if (name in exports.service_usage){
			if (!exports.service_usage[name]) return null;
			else return exports.service_usage[name];
		} else {
			return null;
		}
	},
};

