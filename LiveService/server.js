// server.js

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var redis = require('redis');
var mongoose = require('mongoose');
var exec = require('child_process').exec;
var responseTime = require('response-time');
var errorHandler = require('errorhandler');
var util = require("./app/logics/libs/Common.js");

util.getIP();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());




app.use(responseTime(function (req, res, time) {
    var stat = (req.url).toLowerCase()
        .replace(/[:\.]/g, '');
    console.log(stat + " " + time);
}));


//setting redis - cache object
var redis_config = require("./Config.js").redis_config;

if (redis_config.enable) {
    var redis_client = redis.createClient(redis_config.port, redis_config.host);
    redis_client.on('connect', function () {
        console.log("Connected to Redis");
    });

}

//setting mongo - persistent database
var mongo_config = require("./Config.js").mongo_config;
if (mongo_config.enable) {
    mongoose.connect(mongo_config.location);
}

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

//MIDDLEWARE DECLARE
require('./middleware.js')(app, router, redis_client);

require('./route.js')(app, router)
if (require("./Config.js").mode == "development") {
    console.log("development")
    app.use(errorHandler({dumpExceptions: true, showStack: true}));
} else {
    app.enable('view cache')
    console.log("production")
    app.use(function (err, req, res, next) {
        res.status(500)
        res.render('error');
        console.log(err)
    })
}


// START THE SERVER
// =============================================================================
process.stdout.write("Try port: ");
var port = process.argv[2];
var tester = createServer(port);
function createServer(port) {
    process.stdout.write(port + " ");
    var tester = app.listen(port)
        .once('error', function (err) {
           	if (err.code == 'EADDRINUSE'){
            	console.log("Port in used")
            	process.exit()
            }
        })
        .once('listening', function () {
            process.stdout.write("\n\tStart listening " + port + " ... \n");
            setTimeout(function () {
                require("./Config.js").setRESTPort(port);
            }, 1000);
        });
}
