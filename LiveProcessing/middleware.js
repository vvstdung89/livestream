var async = require('async');
var SessionService = require("./app/logics/Thrift/SessionService");

function parseCookies(request) {
	var list = {},
		rc = request.headers.cookie;
	rc && rc.split(';').forEach(function( cookie ) {
		var parts = cookie.split('=');
		list[parts.shift().trim()] = decodeURI(parts.join('='));
	});
	return list;
}
exports = module.exports = function (app, router, redis_client){

	router.use(function(req, res, next) {
        res.redis_client = redis_client;
        res.app = app;
        next();
	});
};
