/**
 * Created by david89 on 9/14/2016.
 */
var thrift = require('thrift');
var SessionClient_READ = require("../../../Services/SessionService/SessionRead");

var transport = require('thrift/lib/nodejs/lib/thrift/framed_transport.js');
var protocol  = require('thrift/lib/nodejs/lib/thrift/binary_protocol.js');

var SessionRead = thrift.createClient(SessionClient_READ, thrift.createConnection("mecloud.vn",7814, {
    transport : transport,
    protocol : protocol
}));


exports = module.exports = {
    read: SessionRead,
    type: require("../../../Services/SessionService/memecloud.session.v2_types")
};
