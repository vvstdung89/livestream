var Gearman = require("node-gearman");
var config = require("../../../Config.js")
var util = require("../libs/Common.js")
var hostname = config.gearman_config.host
var port = config.gearman_config.port
var gearman = new Gearman(hostname, port);
gearman.on("connect", gearmanConnected);
gearman.connect();

var exec = require("child_process").exec
var execSync = require("child_process").execSync
function gearmanConnected(){
    gearman.registerWorker("ProcessingLive", function(payload, worker){
        if(!payload){
            worker.error();
            return;
        }

        var str = payload.toString('utf8').replace('\0', '');
        console.log(str);
        worker.error()
        var obj = JSON.parse(str);

        var info = {
            key: obj.videoKey
        }
        info.activity = [{
            time: (new Date()).getTime(),
            name: 6
        }]
        var LiveService = require("../../../Services/LiveService");
        LiveService.update(info, function(err, result){
        })

        // need input folder location, events ([t1,t2,t3,t4])
        // output folder location, 
        // videoId, aliasId, quality,
        // videokey

        var walk    = require('walk');
        var files   = [];

        var walker  = walk.walk(obj.input, { followLinks: false });

        walker.on('file', function(root, stat, next) {
            if (stat.name.indexOf(".ts")==stat.name.length-3){
                if (stat.name.indexOf("_")!=0)
                    files.push({name:stat.name,time:(new Date(stat.mtime)).getTime()});
            }
            next();
        });

        var state = "pause";
        var timeID = 0;
        var concat =  [];
        walker.on('end', function() {
            files.sort(function(a,b){return a.time-b.time})
            console.log(files)
            for (var i = 0; i <  files.length; i++){
                if (state == "pause"){ //check for start stream
                    if (timeID >= obj.events.length || (files[i].time - 2000) < obj.events[timeID]) continue
                    else if (files[i].time >= obj.events[timeID]) {
                        concat.push("concat:"+files[i].name) 
                        state = "stream"
                        timeID++;
                    }
                } else { //check for pause || end stream
                    if (timeID >= obj.events.length || (files[i].time+2000) < obj.events[timeID]) {
                        concat.push("concat:"+files[i].name) 
                    } else if (files[i].time >= obj.events[timeID]) {
                        concat.push("concat:"+files[i].name) 
                        state = "pause"
                        timeID++;
                    }
                }
            }
            
            
            execSync("runuser -l zdeploy -c 'mkdir -p /data/cloud/waiting_convert/"+obj.output+"'" );
            var group = 300;
            var async = require("async")
            var semaphore = require("semaphore")(4)
            var counter = 0;
            var divideFiles = [];
            function join(A, start, end, depth, callback){
                if (end - start > group) {
                    async.auto({
                        left: function(callback){
                            join(A, start, Math.floor((start+end)/2), depth+1, function(){
                                callback()
                            });
                        }, 
                        right: function(callback){
                            join(A, Math.ceil((start+end)/2), end, depth+1,function(){
                                callback()
                            });
                        }, 
                    }, function(err, result){
                        if (depth==0){
                            var time = (new Date()).getTime()
                            divideFiles.sort()
                            console.log(divideFiles.join("|"))
                            exec('cd ' + obj.input + ' && ffmpeg -y -i "' + divideFiles.join("|") +'" -vcodec copy /data/cloud/waiting_convert/' + obj.output + "/"+ time+ ".mp4", function(err, stdout, stderr){
                                callback(time+ ".mp4")
                            });
                        } else {
                            callback()
                        }
                        
                    })
                } else {
                    semaphore.take(function(){
                        var concatStr = concat.slice(start, end).join("|");
                        var time = (new Date()).getTime()
                        if (depth==0){
                            console.log("depth 0 " + concatStr)
                            exec('cd ' + obj.input + ' && ffmpeg -y -i "' + concatStr +'" -vcodec copy /data/cloud/waiting_convert/' + obj.output + "/"+ time+ ".mp4", function(err, stdout, stderr){
                                semaphore.leave()
                                callback(time + ".mp4")
                            });
                        } else {
                            console.log("depth 1 " + concatStr)
                            exec('cd ' + obj.input + ' && ffmpeg -y -i "' + concatStr +'" -c copy ' +  "_" + time+ ".ts", function(err, stdout, stderr){
                                divideFiles.push("concat:" + "_" + time + ".ts");
                                semaphore.leave()
                                callback()
                            });
                        }
                        
                    })
                    
                }
            }

            join(concat,0,concat.length-1, 0, function(name){
                var info = {
                    key: obj.videoKey
                }
                info.activity = [{
                    time: (new Date()).getTime(),
                    name: 7
                }]
                var LiveService = require("../../../Services/LiveService");
                LiveService.update(info, function(err, result){
                })

                var info = {
                    videoKey : obj.videoKey,
                    filePath: obj.output + "/"+ name,
                    quality: obj.quality,
                    aliasId: obj.aliasId
                }    

                console.log(info)
                gearman.submitJob("CloudConvertClip", JSON.stringify(info),false,{background:true});
            })
        });
    })
}

