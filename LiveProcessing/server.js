// server.js

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var redis = require('redis');
var mongoose = require('mongoose');
var responseTime = require('response-time');
var errorHandler = require('errorhandler');
var util = require("./app/logics/libs/Common.js");
var server = require('http').createServer(app);  
var io = require('socket.io')(server);
var fs = require('fs');

// var data1 = fs.readFileSync('/root/teststr');
//
// io.on('connection', function(client) {
//     console.log('Client connected...');
//
//     client.on('join', function(data) {
//
//         client.emit('messages', '123456');
//         setInterval(function(){
//             console.log(new Date());
//             fs.readFile('/root/123.mp4', function(err, buf){
//                 console.log(new Date());
//                 console.log(buf);
//                 // client.emit('messages', buf);
//             });
//         },1000);
        // setInterval(function(){
        //     fs.readFile('/root/sort.csv', function(err, buf){
        //         // client.emit('messages', buf);
        //     });     
        // },100)
        // setInterval(function(){
        //     fs.readFile('/root/sort.csv', function(err, buf){
        //         // client.emit('messages', buf);
        //     });     
        // },100)
        // setInterval(function(){
        //     fs.readFile('/root/sort.csv', function(err, buf){
        //         // client.emit('messages', buf);
        //     });     
        // },100)
        // setInterval(function(){
        //     fs.readFile('/root/sort.csv', function(err, buf){
        //         // client.emit('messages', buf);
        //     });     
        // },100)
           
//     });
// });

// server.listen(6758);

util.getIP();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.set('views', './app/templates');
app.set('view engine', 'jade');
app.use("/live",express.static(process.cwd() + '/app/public'));




app.use(responseTime(function (req, res, time) {
    var stat = (req.url).toLowerCase()
        .replace(/[:\.]/g, '');
    console.log(stat + " " + time);
}));


//setting redis - cache object
var redis_config = require("./Config.js").redis_config;

if (redis_config.enable) {
    var redis_client = redis.createClient(redis_config.port, redis_config.host);
    redis_client.on('connect', function () {
        console.log("Connected to Redis");
    });

}

//setting mongo - persistent database
var mongo_config = require("./Config.js").mongo_config;
if (mongo_config.enable) {
    mongoose.connect(mongo_config.location);
}

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

//MIDDLEWARE DECLARE
require('./middleware.js')(app, router, redis_client);

require('./route.js')(app, router);
if (require("./Config.js").mode == "development") {
    console.log("development");
    app.use(errorHandler({dumpExceptions: true, showStack: true}));
} else {
    app.enable('view cache');
    console.log("production");
    app.use(function (err, req, res, next) {
        res.status(500);
        res.render('error');
        console.log(err)
    })
}


// START THE SERVER
// =============================================================================

var port = process.argv[2];

if (port) {
    process.stdout.write("Try port: ");
    createServer(port);
}
function createServer(port) {
    process.stdout.write(port + " ");
        app.listen(port)
        .once('error', function (err) {
           	if (err.code == 'EADDRINUSE'){
            	console.log("Port in used");
            	process.exit()
            }
        })
        .once('listening', function () {
            process.stdout.write("\n\tStart listening " + port + " ... \n");
            setTimeout(function () {
                require("./Config.js").setRESTPort(port);
            }, 1000);
        });
}
